// Arduino Signal Filtering Library
// Copyright 2012-2013 Jeroen Doggen (jeroendoggen@gmail.com)

#ifndef butterFilter_h
#define butterFilter_h
#include <Arduino.h>
#include <Filter.h>

//Low pass butterworth filter order=4 alpha1=0.1 
class butterFilter
{
	public:
		butterFilter();
		void begin();
		short run(short data);
		
//	private:
	protected:
		short v[5];

};
#endif
