// Arduino Signal Filtering Library
// Copyright 2012-2013 Jeroen Doggen (jeroendoggen@gmail.com)

#include <Arduino.h>
#include <butterFilter.h>

/// Constructor
butterFilter::butterFilter()
{
	for(int i=0; i <= 4; i++)
	v[i]=0;
}

/// Begin function: set default filter options
void butterFilter::begin()
{
}

/// Just a placeholder
short butterFilter::run(short x)
{
	v[0] = v[1];
	v[1] = v[2];
	v[2] = v[3];
	v[3] = v[4];
	long tmp = ((((x *  80939L) >>  9)	//= (   4.8243433577e-3 * x)
				+ ((v[0] * -98241L) >> 4)	//+( -0.1873794924*v[0])
				+ ((v[1] * 69119L) >> 1)	//+(  1.0546654059*v[1])
				+ (v[2] * -75825L)	//+( -2.3139884144*v[2])
				+ (v[3] * 77644L)	//+(  2.3695130072*v[3])
				)+16384) >> 15; // round and downshift fixed point /32768

			v[4]= (short)tmp;
			return (short)((/* xpart *//*1.000000 1.000000 */
				(((v[0] + v[4]))<<12) /* 65536L (^2)*/	//  (v[0] + v[4])/*4.000000 4.000000 */
				+(((v[1] + v[3]))<<14) /* 65536L (^2)*/	//  + 4 * (v[1] + v[3])
				+(98304L * v[2]))	//  + 6 * v[2]
			+8192) >> 13; // round and downshift fixed point
}
