clear all;
close all;
clc;

canspeed=500;
freq=uint8(16);
SJW=uint8(1);
  
NBT = 1/(canspeed)*1000.0;
  for BRP = 0:7
    TQ = 2*(uint8(BRP)+1)/freq;
    tempBT = NBT / TQ;
      if(tempBT<=25)
        BT = tempBT;
        if(tempBT-BT==0)
            break
        end
      end
  end
  
  SPT = uint8(0.7 * BT);
  PRSEG = uint8((SPT - 1) / 2);
  PHSEG1 = uint8(SPT - PRSEG - 1);
  PHSEG2 = uint8(BT - PHSEG1 - PRSEG - 1);

  if(PRSEG + PHSEG1 < PHSEG2)
      warning('False');
  end
  if(PHSEG2 <= SJW)
      warning('False');
  end
  
  BTLMODE = 1;
  SAM = 0;
  
  % Set registers
  data = bitor((bitshift((SJW-1),6)),BRP);
  CNF1 = data;
  CNF2 = bitor(bitor(bitshift(BTLMODE,7),bitshift(SAM,6)),bitor(bitshift(uint8(PHSEG1-1),3),uint8(PRSEG-1)));
  CNF3 = bitor(bin2dec('10000000'),uint8(PHSEG2-1));

  dec2hex(CNF1)
  dec2hex(CNF2)
  dec2hex(CNF3)